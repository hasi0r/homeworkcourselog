﻿using System;

namespace HomeworkCourseLog
{
    internal class Student
    {
        public Student(string pesel, string name, string surname, DateTime birthDate, Sexes sex)
        {
            Pesel = pesel;
            Name = name;
            Surname = surname;
            BirthDate = birthDate;
            Sex = sex;
        }
        public string Pesel { get; }
        public string Name { get; }
        public string Surname { get; }
        public DateTime BirthDate { get; }
        public Sexes Sex { get; }
    }
    public enum Sexes
    {
        Kobieta = 1,
        Mężczyzna = 2
    }
}
