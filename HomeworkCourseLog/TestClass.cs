﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog
{
    internal class TestClass
    {
        public static CourseLog TestData()
        {
            CourseLog courseLog;
            List<Student> testStudents = new List<Student>();
            testStudents.Add(new Student("99999999999", "Kamil", "Pesta", new DateTime(1987, 05, 12), Sexes.Mężczyzna));
            testStudents.Add(new Student("88888888888", "Janusz", "Kowalski", new DateTime(1975, 01, 05), Sexes.Mężczyzna));
            testStudents.Add(new Student("77777777777", "Maryska", "Gąbka", new DateTime(1992, 12, 30), Sexes.Kobieta));
            testStudents.Add(new Student("66666666666", "Tadeusz", "Nowak", new DateTime(1981, 07, 10), Sexes.Mężczyzna));

            courseLog = new CourseLog("Kurs junior C# Developer", new CourseLeader("Kuba", "Bulczak"), new DateTime(2017, 9, 25),60.0, 50.0, testStudents);
            Dictionary<string, bool> testDay1 = new Dictionary<string, bool>
            {
                { "99999999999", true },
                { "88888888888", true },
                { "77777777777", true },
                { "66666666666", true }
            };
            Dictionary<string, bool> testDay2 = new Dictionary<string, bool>
            {
                { "99999999999", true },
                { "88888888888", true },
                { "77777777777", true },
                { "66666666666", true }
            };
            Dictionary<string, bool> testDay3 = new Dictionary<string, bool>
            {
                { "99999999999", true },
                { "88888888888", false },
                { "77777777777", true },
                { "66666666666", false }
            };
            Dictionary<string, bool> testDay4 = new Dictionary<string, bool>
            {
                { "99999999999", false },
                { "88888888888", true },
                { "77777777777", false },
                { "66666666666", true }
            };
            Dictionary<string, bool> testDay5 = new Dictionary<string, bool>
            {
                { "99999999999", true },
                { "88888888888", true },
                { "77777777777", false },
                { "66666666666", true }
            };
            Dictionary<string, bool> testDay6 = new Dictionary<string, bool>
            {
                { "99999999999", false },
                { "88888888888", false },
                { "77777777777", true },
                { "66666666666", true }
            };
            Dictionary<string, bool> testDay7 = new Dictionary<string, bool>
            {
                { "99999999999", true },
                { "88888888888", true },
                { "77777777777", true },
                { "66666666666", false }
            };
            Dictionary<string, bool> testDay8 = new Dictionary<string, bool>
            {
                { "99999999999", false },
                { "88888888888", true },
                { "77777777777", true },
                { "66666666666", false }
            };
            Dictionary<string, bool> testDay9 = new Dictionary<string, bool>
            {
                { "99999999999", false },
                { "88888888888", true },
                { "77777777777", false },
                { "66666666666", true }
            };
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 10), testDay1));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 11), testDay2));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 12), testDay3));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 13), testDay4));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 14), testDay5));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 15), testDay6));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 16), testDay7));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 17), testDay8));
            courseLog.ClassDays.Add(new ClassDay(new DateTime(2017, 10, 18), testDay9));

            Dictionary<string, double> points1 = new Dictionary<string, double>()
            {
                { "99999999999", 76 },
                { "88888888888", 25 },
                { "77777777777", 56 },
                { "66666666666", 32 }
            };
            Dictionary<string, double> points2 = new Dictionary<string, double>()
            {
                { "99999999999", 24 },
                { "88888888888", 76 },
                { "77777777777", 56 },
                { "66666666666", 98 }
            };
            Dictionary<string, double> points3 = new Dictionary<string, double>()
            {
                { "99999999999", 90 },
                { "88888888888", 32 },
                { "77777777777", 67 },
                { "66666666666", 32 }
            };
            Dictionary<string, double> points4 = new Dictionary<string, double>()
            {
                { "99999999999", 46 },
                { "88888888888", 96 },
                { "77777777777", 47 },
                { "66666666666", 84 }
            };
            Dictionary<string, double> points5 = new Dictionary<string, double>()
            {
                { "99999999999", 76 },
                { "88888888888", 25 },
                { "77777777777", 56 },
                { "66666666666", 32 }
            };
            Dictionary<string, double> points6 = new Dictionary<string, double>()
            {
                { "99999999999", 28 },
                { "88888888888", 55 },
                { "77777777777", 45 },
                { "66666666666", 87 }
            };
            Dictionary<string, double> points7 = new Dictionary<string, double>()
            {
                { "99999999999", 46 },
                { "88888888888", 54 },
                { "77777777777", 34 },
                { "66666666666", 87 }
            };
            courseLog.Homeworks.Add(new Homework(100, points1));
            courseLog.Homeworks.Add(new Homework(100, points2));
            courseLog.Homeworks.Add(new Homework(100, points3));
            courseLog.Homeworks.Add(new Homework(100, points4));
            courseLog.Homeworks.Add(new Homework(100, points5));
            courseLog.Homeworks.Add(new Homework(100, points6));
            courseLog.Homeworks.Add(new Homework(100, points7));

            return courseLog;
        }
    }
}
