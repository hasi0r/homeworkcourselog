﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog
{
    public class DataReader
    {
        private readonly int[] peselMultipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

        public int ReadIntNumber()
        {
            string input = Console.ReadLine();
            int result;
            while (!(int.TryParse(input, out result)))
            {
                Console.WriteLine("Nieprawidłowe dane, podaj liczbę całkowitą");
                input = Console.ReadLine();
            }
            return result;
        }
        public int ReadIntNumber(int min, int max)
        {
            string input = Console.ReadLine();
            int result;
            while (!(int.TryParse(input, out result)) || result < min || result > max)
            {
                Console.WriteLine($"Podaj liczbę całkowitą w zakresie od {min} do {max}");
                input = Console.ReadLine();
            }
            return result;
        }
        public bool ReadBoolean()
        {
            string input = ReadIntNumber(0, 1).ToString();

            if (input == "1")
            {
                input = "true";
            }
            else
            {
                input = "false";
            }
            bool result = Convert.ToBoolean(input);
            return result;
        }
        public double ReadDoubleNumber()
        {
            string input = Console.ReadLine();
            double result;
            while (!(double.TryParse(input, out result)))
            {
                Console.WriteLine("Nieprawidłowe dane, podaj liczbę");
                input = Console.ReadLine();
            }
            return result;
        }
        public double ReadDoubleNumber(double min, double max)
        {

            string input = Console.ReadLine();
            double result;
            while (!(double.TryParse(input, out result)) || result < min || result > max)
            {
                Console.WriteLine($"Podaj liczbę w zakresie od {min} do {max}");
                input = Console.ReadLine();
            }
            return result;
        }
        public DateTime ReadDateTime()
        {
            string input = Console.ReadLine();
            var format = "dd.MM.yyyy";
            DateTime result;
            while (!(DateTime.TryParseExact(input,format, CultureInfo.InvariantCulture,DateTimeStyles.None, out result)))
            {
                Console.WriteLine($"Podaj poprawny format daty({format}).");
                input = Console.ReadLine();
            }
            return result;
        }

        /// <summary>
        /// Asking user to answer message and returns answer in string
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string ReadStringWithMessage(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        /// <summary>
        /// returns validated pesel number given by console user
        /// </summary>
        /// <returns></returns>
        public string ReadPesel()
        {
            string input = Console.ReadLine();
            while (input.Length != 11 || !input.All(char.IsDigit)
                || !ValidatePesel(input))
            {
                Console.WriteLine("Nieprawidłowe dane, podaj porawny nr Pesel");
                input = Console.ReadLine();
            }
            return input;
        }
        private bool ValidatePesel(string pesel)
        {
            bool result = (CountControlSumForPesel(pesel) == (int)char.GetNumericValue(pesel[10]));
            return result;
        }
        private int CountControlSumForPesel(string pesel)
        {
            int controlSum = 0;
            for (int i = 0; i < peselMultipliers.Length; i++)
            {

                controlSum += (int)char.GetNumericValue(pesel[i]) * peselMultipliers[i];
            }
            int rest = controlSum % 10;
            rest = 10 - rest;
            rest %= 10;
            return rest;
        }
    }
}
