﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog
{
    internal class ConsoleInput
    {
        private Student GetStudentInfoFromUser()
        {
            DataReader dataReader = new DataReader();
            Console.WriteLine("----------------------------\n- Podaj nr PESEL studenta:");
            string pesel = dataReader.ReadPesel();
            Console.WriteLine("- Podaj imię studenta/ki:");
            string name = Console.ReadLine();
            Console.WriteLine("- Podaj nazwisko studenta/ki:");
            string surname = Console.ReadLine();
            Console.WriteLine("- Podaj datę urodzenia studenta/ki:");
            DateTime birthDate = dataReader.ReadDateTime();
            Console.WriteLine("- Podaj płeć studenta/ki");
            UserMenu sexMenu = new UserMenu(new string[] { Sexes.Kobieta.ToString(), Sexes.Mężczyzna.ToString() });
            Sexes sex = (Sexes)sexMenu.AskUser();

           return new Student(pesel, name, surname, birthDate, sex);

        }

        /// <summary>
        /// Asking to fill console form about new course, and students 
        /// </summary>
        /// <returns></returns>
        public CourseLog GenerateNewCourseLog()
        {
            CourseLog courseLog;
            DataReader dataReader = new DataReader();

            Console.WriteLine("- Podaj nazwę kursu");
            string courseName = Console.ReadLine();
            CourseLeader courseLeader = new CourseLeader(dataReader.ReadStringWithMessage("- Podaj imię prowadzącego"),
            dataReader.ReadStringWithMessage("- Podaj nazwisko prowadzącego"));

            Console.WriteLine("- Podaj datę rozpoczęcia kursu");
            DateTime startDate = dataReader.ReadDateTime();

            Console.WriteLine("- Podaj próg zaliczania prac domowyc(w procentach)");
            double homeworkPercentMinimalScore = dataReader.ReadDoubleNumber(0, 100);
            Console.WriteLine("- Podaj próg obecności(w procentach)");
            double presencePercentMinimalScore = dataReader.ReadDoubleNumber(0, 100);

            Console.WriteLine("- Podaj liczbę studentów");
            int numberOfStudents = dataReader.ReadIntNumber();

            List<Student> studentsList = new List<Student>();
            for (int i = 0; i < numberOfStudents; i++)
            {
                studentsList.Add(GetStudentInfoFromUser());
            }
            courseLog = new CourseLog(courseName, courseLeader, startDate, homeworkPercentMinimalScore, presencePercentMinimalScore, studentsList);

            return courseLog;
        }

        /// <summary>
        /// Asking user to fill console form about presence for all student in course and adds it to courseLog
        /// </summary>
        /// <param name="courseLog"></param>
        public Dictionary<string, bool> CheckPressence(List<Student> students)
        {
            DataReader dataReader = new DataReader();
           
            Dictionary<string, bool> presence = new Dictionary<string, bool>();
            foreach (var student in students)
            {
                Console.WriteLine($"Czy {student.Name} {student.Surname} Jest obecny?(1 - tak, 0 - nie)");
                presence.Add(student.Pesel, dataReader.ReadBoolean());
            }
            return presence;
        }

        /// <summary>
        /// Ask user for date and checks if its not before course start date
        /// </summary>
        /// <param name="courseStartDate"></param>
        /// <returns></returns>
        public DateTime AskUserForDate( DateTime courseStartDate)
        {
            DataReader dataReader = new DataReader();

            Console.WriteLine("- Podaj datę obecności");
            DateTime date = dataReader.ReadDateTime();
            while (date.CompareTo(courseStartDate) < 0)
            {
                Console.WriteLine("- Data nie może być wcześniejsza niż data rozpoczęcia kursu !!");
                date = dataReader.ReadDateTime();
            }
            return date;
        }

        /// <summary>
        ///   Asking user to fill console form about homework for all student in course, returns Homework
        /// </summary>
        /// <param name="courseLog"></param>
        public Homework AddHomework(List<Student> studentsList)
        {
            DataReader dataReader = new DataReader();

            Console.WriteLine("Podaj maksymalną ilośćpunktów");
            int maxPointsPossible = dataReader.ReadIntNumber();

            Dictionary<string, double> pointsOfStudents = new Dictionary<string, double>();
            foreach (var student in studentsList)
            {
                Console.WriteLine($"Podaj liczbę punktów uzyskaną przez: {student.Name} {student.Surname}:");
                pointsOfStudents.Add(student.Pesel, dataReader.ReadDoubleNumber(0, maxPointsPossible));
            }
            return new Homework(maxPointsPossible, pointsOfStudents);
        }
    }
}
