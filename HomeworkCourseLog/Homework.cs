﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog
{
    internal class Homework
    {
        public Homework(double maxPointsPossible, Dictionary<string, double> pointsOfStudents)
        {
            MaxPointsPossible = maxPointsPossible;
            PointsOfStudents = pointsOfStudents;
        }

        public double MaxPointsPossible { get; set; }
        public Dictionary<string, double> PointsOfStudents { get; set; }

    }
}
