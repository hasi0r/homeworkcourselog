﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog
{
    internal class CourseLog
    {
        public CourseLog(string courseName, CourseLeader courseLeader, DateTime startDate, double homeworkPercentMinimalScore,
            double presencePercentMinimalScore, List<Student> studentList)
        {
            StudentList = studentList;
            CourseName = courseName;
            CourseLeader = courseLeader;
            StartDate = startDate;
            HomeworkPercentMinimalScore = homeworkPercentMinimalScore;
            PresencePercentMinimalScore = presencePercentMinimalScore;
            ClassDays = new List<ClassDay>();
            Homeworks = new List<Homework>();
        }

        public string CourseName { get; set; }
        public CourseLeader CourseLeader { get; set; }
        public DateTime StartDate { get; set; }
        public double HomeworkPercentMinimalScore { get; set; }
        public double PresencePercentMinimalScore { get; set; }
        public List<Student> StudentList { get; set; }
        public List<ClassDay> ClassDays { get; set; }
        public List<Homework> Homeworks { get; set; }
    }
}
