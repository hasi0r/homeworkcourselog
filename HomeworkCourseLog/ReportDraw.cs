﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog
{
    internal class ReportDraw
    {

        public static void HeadRaport(CourseLog courseLog)
        {

            int tableWidth = 61;
            PrintLine(tableWidth);
            PrintRow(new string[] { "Nazwa kursu", "Data rozpoczęcia kursu" }, tableWidth);
            PrintLine(tableWidth);
            PrintRow(new string[] { courseLog.CourseName, courseLog.StartDate.ToShortDateString() }, tableWidth);
            PrintLine(tableWidth);
            PrintRow(new string[] { "Próg obecności", "Próg z pracy domowej" }, tableWidth);
            PrintLine(tableWidth);
            PrintRow(new string[] { courseLog.PresencePercentMinimalScore.ToString() + "%", courseLog.HomeworkPercentMinimalScore.ToString() + "%" },
                tableWidth);
            PrintLine(tableWidth);

        }
        public static void StudentsRaport(CourseLog course)
        {
            int tableWidth = Console.BufferWidth - 2;
            PrintLine(tableWidth);
            PrintRow(new string[] { "Student", "Obecności", "Prace domowe" }, tableWidth);
            PrintLine(tableWidth);

            int maxPresencePoints = course.ClassDays.Count;
            double maxHomeworkPoints = 0;
            foreach (var homework in course.Homeworks)
            {
                maxHomeworkPoints += homework.MaxPointsPossible;
            }

            //students
            foreach (var student in course.StudentList)
            {
                string[] rows = new string[3];
                rows[0] = String.Concat(student.Name, " ", student.Surname);

                //counting presence
                int actualPresence = 0;
                foreach (var classDay in course.ClassDays)
                {
                    bool isPresent = classDay.Presence[student.Pesel];
                    if (isPresent == true)
                    {
                        actualPresence += 1;
                    }
                }

                double presenceProcent = (maxPresencePoints != 0 ? (double)actualPresence / (double)maxPresencePoints * 100.0 : 0);
                string presencePasssed;
                if (presenceProcent >= course.PresencePercentMinimalScore)
                {
                    presencePasssed = "Zaliczone";
                }
                else
                {
                    presencePasssed = "Niezaliczone";
                }

                //counting homework points
                double earnedPoints = 0.0;
                foreach (var homework in course.Homeworks)
                {
                    earnedPoints += homework.PointsOfStudents[student.Pesel];
                }
                double homeworkPointsProcent = (maxHomeworkPoints != 0 ? earnedPoints / maxHomeworkPoints * 100.0 : 0);
                string homeworkPassed;
                if (homeworkPointsProcent >= course.HomeworkPercentMinimalScore)
                {
                    homeworkPassed = "Zaliczone";
                }
                else
                {
                    homeworkPassed = "Niezaliczone";
                }

                //writing rows on console
                rows[1] = $"{actualPresence.ToString()}/{maxPresencePoints}({presenceProcent:F}%) - {presencePasssed}";
                rows[2] = $"{earnedPoints.ToString()}/{maxHomeworkPoints}({homeworkPointsProcent:F}%) - {homeworkPassed}";
                PrintRow(rows, tableWidth);
                PrintLine(tableWidth);
            }
        }
        static void PrintLine(int width)
        {
            Console.WriteLine(new string('-', width));
        }
        static void PrintRow(string[] columns, int tableWidth)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCenter(column, width) + "|";
            }
            Console.WriteLine(row);
        }
        static string AlignCenter(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }

    }
}
