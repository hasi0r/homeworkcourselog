﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] options = { "Dodaj kurs", "Dodaj dzień zajęć", "Dodaj pracę domową", "Wyświetl raport", "Wyjście" };
            UserMenu startMenu = new UserMenu(options);
            CourseLog courseLog = null;

            bool exit = false;

            /************************
             * 
             * testy 
             * 
             * ****************/
            // courseLog = TestClass.TestData();

            while (!exit)
            {

                Console.WriteLine("Wybierz działanie:");
                int option = startMenu.AskUser();
                ConsoleInput consoleInput = new ConsoleInput();
                switch (option)
                {
                    case 1:
                        if (courseLog == null)
                        {
                            courseLog = consoleInput.GenerateNewCourseLog();
                        }
                        else
                        {
                            Console.WriteLine("Kurs jest już założony");
                        }
                        break;
                    case 2:
                        if (courseLog != null)
                        {
                           DateTime date = consoleInput.AskUserForDate(courseLog.StartDate);
                           Dictionary<string,bool> presence =  consoleInput.CheckPressence(courseLog.StudentList);
                           courseLog.ClassDays.Add(new ClassDay(date, presence));
                        }
                        else
                        {
                            Console.WriteLine("Brak kursu, załóż kurs.");
                        }
                        break;
                    case 3:
                        if (courseLog != null)
                        {
                           Homework homework = consoleInput.AddHomework(courseLog.StudentList);
                            courseLog.Homeworks.Add(homework);
                        }
                        else
                        {
                            Console.WriteLine("Brak kursu, załóż kurs.");
                        }
                        break;
                    case 4:
                        if (courseLog != null)
                        {
                            ReportDraw.HeadRaport(courseLog);
                            ReportDraw.StudentsRaport(courseLog);
                        }
                        else
                        {
                            Console.WriteLine("Brak kursu, załóż kurs.");
                        }
                        break;
                    case 5:
                        exit = true;
                        break;
                }
            }
        }
    }
}
