﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog
{
    public class ClassDay
    {
        public ClassDay(DateTime date, Dictionary<string, bool> presence)
        {
            Date = date;
            Presence = presence;
        }

        public DateTime Date { get; set; }
        public Dictionary<string,bool> Presence { get; set; }
    }
}
