﻿namespace HomeworkCourseLog
{
    internal class CourseLeader
    {
        public CourseLeader(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
